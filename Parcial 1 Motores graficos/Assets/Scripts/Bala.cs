using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public float speed = 8f;
    public float duraciondeVida = 2f;
    float tiempodeVida;
    public int ataque = 10;

    private void Start()
    {
        tiempodeVida = duraciondeVida;
    }
    private void Update()
    {
        tiempodeVida -= Time.deltaTime;
        if (tiempodeVida <= 0)
        {
            gameObject.SetActive(false);

        }

    }
    private void FixedUpdate()
    {
        transform.position += transform.forward * speed * Time.fixedDeltaTime;
    }

}