using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public Transform posArma;
    public Transform cam;
    public GameObject balaPrefab;
    RaycastHit golpe;
    public LayerMask ignoreLayer;

    private void Update()
    {
       

        if (Input.GetMouseButtonDown(0))
        {


            GameObject objetoBala = Instantiate(balaPrefab);
            objetoBala.transform.position = posArma.position;
            if(Physics.Raycast(cam.position,cam.forward,out golpe, Mathf.Infinity, ~ignoreLayer)) {
                objetoBala.transform.LookAt(golpe.point);
            }
            else
            {
                Vector3 dir = cam.position + cam.forward * 10f;
                objetoBala.transform.LookAt(dir);
            }
          

        }

    }
}