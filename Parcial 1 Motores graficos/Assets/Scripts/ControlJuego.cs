using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class ControlJuego : MonoBehaviour
{
    public float timer = 0;
    public TextMeshProUGUI textoGameOver;
    public TextMeshProUGUI textoTimerPro;
    public Image reticula;
    public Image pantallaNegra;
   
   
    // Update is called once per frame

  
    public void Update()
    {
        timer -= Time.deltaTime;
        textoGameOver.gameObject.SetActive(false);
        textoTimerPro.text = "" + timer.ToString("f0");
        pantallaNegra.gameObject.SetActive(false);

        if (timer <= 1)
        {
            textoGameOver.gameObject.SetActive(true);
            Destroy(textoTimerPro);
            reticula.gameObject.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            pantallaNegra.gameObject.SetActive(true);
            Time.timeScale = 0;
        }

    }

    public void reiniciarEscena()
    {
        SceneManager.LoadScene("SampleScene");
        Time.timeScale = 1;
    }
}