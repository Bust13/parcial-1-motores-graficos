using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class Puerta : MonoBehaviour
{
    public WinLLave gano;
    public bool llave;
    public TextMeshProUGUI textoPerdio;
    public TextMeshProUGUI textoTimer;
    public TextMeshProUGUI textoGanaste;
   
    public Image ret;
    public Image pantBlanca;
    public Image pantngra;
    public Button rein;

    private void Start()
    {
        pantBlanca.gameObject.SetActive(false);
        textoGanaste.gameObject.SetActive(false);
        rein.gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {


        if (gano)
        {

            textoPerdio.gameObject.SetActive(false);
           Destroy(textoTimer);
            ret.gameObject.SetActive(false);
            rein.gameObject.SetActive(true);
            textoGanaste.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            pantBlanca.gameObject.SetActive(true);
            pantngra.gameObject.SetActive(false);
            Time.timeScale = 0;
        }

    }
    public void reiniciarEscena()
    {
        SceneManager.LoadScene("SampleScene");
        Time.timeScale = 1;
    }
}

