


using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlJugador : MonoBehaviour

{
    Rigidbody rb;
    float xInicial, yInicial, zInicial;
    public int maximoDeSaltos = 2;
    public int salto = 0;
    public float fuerza = 10f;
    public float rapidezDesplazamiento = 10.0f;
    public bool detectaPiso;
    public Camera camaraPrimeraPersona;
    void Start()
    { 
        Cursor.lockState = CursorLockMode.Locked;
        rb= GetComponent<Rigidbody>();
        xInicial = transform.position.x;
        yInicial = transform.position.y;
        zInicial = transform.position.z;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;

        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        Vector3 Piso = transform.TransformDirection(Vector3.down);
        if ((Input.GetButtonDown("Jump")) && (salto > 0))
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            salto -= 1;
                

        }
        if (Input.GetKeyDown("r")){

            SceneManager.LoadScene("SampleScene");
            Time.timeScale = 1;

        }

     

    }
   

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = true;
            salto = maximoDeSaltos;


        }
        else if (collision.gameObject.CompareTag("Enemigo"))
        {

            transform.position = new Vector3(xInicial, yInicial, zInicial);

        }




    }
    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = false;
           


        }



    }
    
    










}
